import axios, {AxiosInstance, Method} from 'axios';

export class ApiHelper {
  static readonly API_URL = 'http://localhost:3000/players/';

  axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: ApiHelper.API_URL,
      timeout: 1000,
      headers: { 'Content-Type': 'application/json' }
    })
  }

  async request({ method = 'GET' as Method, url = ApiHelper.API_URL, data = null} = {}) {
    const response: any = await this.axiosInstance.request({
      method, url, data
    }).catch((error) => {
      console.error(error);
    });

    if(!response || response.status !== 200) {
      console.error(`problem with request (method: ${method}, url: ${url}, data: ${data}`);
    }

    return response;
  }
}
