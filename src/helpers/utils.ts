export function clamp(num: number, lower: number, upper: number) {
  return Math.min(Math.max(num, lower), upper);
}

export function delay(ms) {
  return new Promise(function (resolve) {
    setTimeout(resolve, ms);
  });
}
