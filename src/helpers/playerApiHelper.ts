import {ApiHelper} from "./apiHelper";

export class PlayerApiHelper extends ApiHelper {
  constructor() {
    super();
  }

  async requestCreatePlayer({name, wins = 0}) {
    return this.request({
      method: 'POST',
      data: {name, wins}
    })
  }

  async requestUpdatePlayer({id, name = null, wins = null}) {
    return this.request({
      url: ApiHelper.API_URL + id,
      method: 'PUT',
      data: {name, wins}
    })
  }

  async requestDestroyPlayer({id}) {
    return this.request({
      url: ApiHelper.API_URL + id,
      method: 'DELETE'
    })
  }
}
