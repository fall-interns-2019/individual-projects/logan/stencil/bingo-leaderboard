import {Component, h, Prop} from "@stencil/core";
import {AppHome} from "../app-home/app-home";
import {delay} from "../../helpers/utils";

@Component({
  tag: 'player-create',
  styleUrl: 'player-create.css'
})
export class PlayerCreate {
  @Prop() modalController: HTMLIonModalControllerElement;

  private loadingController: HTMLIonLoadingControllerElement;
  private form: HTMLFormElement;
  private nameValue: string;
  private winsValue: number = 0;

  private async onCancelClick() {
    await this.modalController.dismiss({
      'cancelled': true
    })
  }

  private async onCreateClick() {
    if(!this.form.reportValidity())
      return;

    const loadingElement = await this.loadingController.create({
      message: 'Creating...',
      spinner: 'crescent'
    });

    await loadingElement.present();

    const {data} = await AppHome.playerApi.requestCreatePlayer({
      name: this.nameValue,
      wins: this.winsValue
    });

    loadingElement.spinner = data ? loadingElement.spinner : null;
    loadingElement.message = data ? 'Success!' : 'Creation failed';

    await delay(500);
    await loadingElement.dismiss();

    await this.modalController.dismiss(data);
  }

  private onNameChange(event) {
    this.nameValue = event.target.value;
  }

  private async onWinChange(event) {
    if(event.key === 'Enter') {
      return this.onCreateClick();
    }

    let num = Number.parseInt(event.target.value);

    if (event.target.value.trim() === '' || isNaN(num) || num < 0) {
      event.target.value = '';
      return;
    }

    this.winsValue = num;
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-button onClick={() => this.onCancelClick()}>
              Cancel
            </ion-button>
          </ion-buttons>
          <ion-title class="ion-text-center">Create Player</ion-title>
          <ion-buttons slot="end">
            <ion-button onClick={() => this.onCreateClick()}>
              Create
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,

      <ion-loading-controller ref={(el) => this.loadingController = el}/>,

      <ion-content fullscreen={true}>
        <form ref={(el) => this.form = el}>
          <ion-list lines="full" class="ion-no-margin ion-no-padding">
            <ion-item>
              <ion-label style={{'font-size': '1.5em'}} position="stacked">
                Name <ion-text color="danger">*</ion-text>
              </ion-label>
              <ion-input required={true} type="text" maxlength={50}
                         onIonChange={(evt) => this.onNameChange(evt)}/>
            </ion-item>

            <ion-item>
              <ion-label style={{'font-size': '1.5em'}} position="stacked">
                Win Count <ion-text color="danger">*</ion-text>
              </ion-label>
              <ion-input required={true} type="number" min="0" max="1000"
                         onKeyUp={(evt) => this.onWinChange(evt)}/>
            </ion-item>
          </ion-list>
        </form>
      </ion-content>
    ]
  }
}
