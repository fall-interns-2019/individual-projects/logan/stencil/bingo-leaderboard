import {Component, h, Listen, State} from "@stencil/core";
import {AppHome} from "../app-home/app-home";
import _ from "underscore";

@Component({
  tag: 'player-list',
  styleUrl: 'player-list.css'
})
export class PlayerList {
  @State() list: any[];
  @State() editing: any = { player: undefined, value: false };

  private modalController: HTMLIonModalControllerElement;
  private toastController: HTMLIonToastControllerElement;
  private listElement: HTMLIonListElement;

  @Listen('playerEvent')
  async handlePlayerEvent(event: CustomEvent) {
    const {mode} = event.detail;

    if (mode === 'editing') {
      const {editingObject} = event.detail;

      this.editing = editingObject;
    } else {
      const {player, index} = event.detail;
      const {id, name, wins} = player;
      let newList = [...this.list];

      if (mode === 'edit') {
        newList[index] = player;
        await AppHome.playerApi.requestUpdatePlayer({id, name, wins});
      } else if (mode === 'delete') {
        newList.splice(index, 1);
        await AppHome.playerApi.requestDestroyPlayer({id});
        await this.listElement.closeSlidingItems();
        this.notify({message: 'Deleted player.'});
      }

      const listChanged = !_.isEqual(this.list, newList);

      if(mode === 'edit' && listChanged) {
        console.log('detected list change');
        await this.listElement.closeSlidingItems();
      }

      this.list = newList.sort((a, b) => {
        return a.wins < b.wins ? 1 : -1
      });
    }
  }

  async componentWillLoad() {
    const {data} = await AppHome.playerApi.request();
    if (!data) return;

    this.list = data.sort((a, b) => { return a.wins < b.wins ? 1 : -1 });
  }

  componentDidLoad() {

  }

  componentWillRender() {

  }

  async componentDidRender() {
    if(this.editing.player && this.editing.winChanged) {
      let slidingItem: HTMLIonItemSlidingElement;

      for(const el of this.listElement.children as any) {
        if(el.player && el.player.id === this.editing.player.id) {
          slidingItem = el.firstChild as HTMLIonItemSlidingElement;
          break;
        }
      }

      if(!slidingItem) return;

      console.log(`opening the sliding item for player ${this.editing.player.name}`);
      await slidingItem.open('end');
    }
  }

  private async notify({message, duration = 2000}) {
    const toast = await this.toastController.create({
      message, duration
    });

    await toast.present();
  }

  private async onCreateClick() {
    const modal = await this.modalController.create({
      component: 'player-create',
      componentProps: {
        'modalController': this.modalController,
        'testText': 'Test success'
      }
    });

    await modal.present();
    await modal.onWillDismiss();
    await this.componentWillLoad();
  }

  private renderListItems() {
    let index = 0;
    return this.list.map((player) => {
      return (
        <player-list-item index={index++} player={player} slideEnabled={!this.editing.value}/>
      )
    })
  }

  render() {
    return [
      <ion-modal-controller ref={(el) => this.modalController = el} />,
      <ion-toast-controller ref={(el) => this.toastController = el} />,
      <ion-list ref={(el) => this.listElement = el} >
        <ion-list-header>
          <ion-label style={{'font-size': '2em'}}>
            Players
          </ion-label>
          <ion-button fill="clear" size="large" shape="round" onClick={() => this.onCreateClick()}>
            Add
            <ion-icon name="person-add" slot="end"/>
          </ion-button>
        </ion-list-header>
        { this.renderListItems() }
      </ion-list>
    ]
  }
}
