import {Component, h} from '@stencil/core';
import {PlayerApiHelper} from "../../helpers/playerApiHelper";

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {

  public static playerApi: PlayerApiHelper = new PlayerApiHelper();

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title class="ion-text-center">Bingo Leaderboard</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class="ion-padding">
        <player-list />
      </ion-content>
    ];
  }
}
