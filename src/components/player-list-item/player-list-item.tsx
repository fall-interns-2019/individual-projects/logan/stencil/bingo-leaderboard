import {Component, Event, Prop, State, h, EventEmitter} from '@stencil/core';
import _ from "underscore";
import {clamp} from "../../helpers/utils";

@Component({
  tag: 'player-list-item',
  styleUrl: 'player-list-item.css'
})
export class PlayerListItem {
  @Prop() index: number;
  @Prop() player: any;
  @Prop() slideEnabled = true;

  @State() editing = false;

  nameValue: string;
  debounce = false;

  @Event({
    eventName: 'playerEvent'
  }) playerEvent: EventEmitter;

  input: HTMLIonInputElement;
  slidingItem: HTMLIonItemSlidingElement;

  componentWillLoad() {

  }

  componentDidLoad() {
    this.nameValue = this.player.name;
  }

  async componentDidRender() {
    if (this.editing) {
      await this.input.setFocus();
    }

    this.debounce = false;
  }

  private async onEditClick() {
    if (this.editing) return;

    this.editing = true;
    this.playerEvent.emit({mode: 'editing', editingObject: { player: this.player, value: true }});
  }

  private async onDeleteClick() {
    if (this.editing) return;

    this.playerEvent.emit({mode: 'delete', index: this.index, player: this.player});
  }

  private async onChangeWinsClick({increase}) {
    this.onEditCompleted(null, {
      wins: clamp(this.player.wins + (increase ? 1 : -1), 0, 10)
    });
  }

  private onInputChanged(event) {
    this.nameValue = event.target.value;
  }

  private async onEditCompleted(event?, newProps?) {
    if (event && event.key !== 'Enter') return;
    if (this.debounce) return;

    this.debounce = true;

    if (newProps) {
      let updatedPlayer = {...this.player};
      _.extendOwn(updatedPlayer, newProps);

      this.player = updatedPlayer;
      this.playerEvent.emit({
        mode: 'edit',
        index: this.index,
        player: this.player
      });
      this.playerEvent.emit({mode: 'editing', editingObject: {
        player: this.player,
        winChanged: newProps.hasOwnProperty('wins')
      }});
    } else {
      this.playerEvent.emit({mode: 'editing', editingObject: { value: false }});
    }

    this.editing = false;
  }

  render() {
    if (!this.editing) {
      return (
        <ion-item-sliding class={this.slideEnabled ? 'faded-out' : ''}
                          ref={(el) => this.slidingItem = el}>
          <ion-item-options side="start">
            <ion-item-option color="warning" onClick={() => this.onEditClick()}>Edit Name</ion-item-option>
          </ion-item-options>

          <ion-item>
            <ion-label>
              {`${this.index + 1}. ${this.player.name}`}
            </ion-label>
            <ion-note slot="end" color={['secondary', 'primary', 'tertiary'][this.index] || 'medium'}>
              { this.player.wins }
            </ion-note>
          </ion-item>

          <ion-item-options side="end">
            <ion-item-option color="medium" onClick={() => this.onChangeWinsClick({increase: true})}>
              <ion-icon name="arrow-dropup" size="large"/>
            </ion-item-option>
            <ion-item-option color="medium" onClick={() => this.onChangeWinsClick({increase: false})}>
              <ion-icon name="arrow-dropdown" size="large"/>
            </ion-item-option>
            <ion-item-option color="danger" onClick={() => this.onDeleteClick()}>Delete</ion-item-option>
          </ion-item-options>
        </ion-item-sliding>
      )
    } else {
      return (
        <ion-item>
          <ion-input ref={(el) => this.input = el as HTMLIonInputElement}
                     type="text" value={this.player.name}
                     onKeyUp={(evt) => {
                       this.onInputChanged(evt);
                       this.onEditCompleted(evt, {name: this.nameValue});
                       console.log('onKeyUp triggered')
                     }}
                     onIonBlur={() => this.onEditCompleted()}
          />
        </ion-item>
      )
    }
  }
}
