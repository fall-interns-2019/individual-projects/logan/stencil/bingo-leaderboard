/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import { HTMLStencilElement, JSXBase } from '@stencil/core/internal';


export namespace Components {
  interface AppHome {}
  interface AppRoot {}
  interface PlayerCreate {
    'modalController': HTMLIonModalControllerElement;
  }
  interface PlayerList {}
  interface PlayerListItem {
    'index': number;
    'player': any;
    'slideEnabled': boolean;
  }
}

declare global {


  interface HTMLAppHomeElement extends Components.AppHome, HTMLStencilElement {}
  var HTMLAppHomeElement: {
    prototype: HTMLAppHomeElement;
    new (): HTMLAppHomeElement;
  };

  interface HTMLAppRootElement extends Components.AppRoot, HTMLStencilElement {}
  var HTMLAppRootElement: {
    prototype: HTMLAppRootElement;
    new (): HTMLAppRootElement;
  };

  interface HTMLPlayerCreateElement extends Components.PlayerCreate, HTMLStencilElement {}
  var HTMLPlayerCreateElement: {
    prototype: HTMLPlayerCreateElement;
    new (): HTMLPlayerCreateElement;
  };

  interface HTMLPlayerListElement extends Components.PlayerList, HTMLStencilElement {}
  var HTMLPlayerListElement: {
    prototype: HTMLPlayerListElement;
    new (): HTMLPlayerListElement;
  };

  interface HTMLPlayerListItemElement extends Components.PlayerListItem, HTMLStencilElement {}
  var HTMLPlayerListItemElement: {
    prototype: HTMLPlayerListItemElement;
    new (): HTMLPlayerListItemElement;
  };
  interface HTMLElementTagNameMap {
    'app-home': HTMLAppHomeElement;
    'app-root': HTMLAppRootElement;
    'player-create': HTMLPlayerCreateElement;
    'player-list': HTMLPlayerListElement;
    'player-list-item': HTMLPlayerListItemElement;
  }
}

declare namespace LocalJSX {
  interface AppHome extends JSXBase.HTMLAttributes<HTMLAppHomeElement> {}
  interface AppRoot extends JSXBase.HTMLAttributes<HTMLAppRootElement> {}
  interface PlayerCreate extends JSXBase.HTMLAttributes<HTMLPlayerCreateElement> {
    'modalController'?: HTMLIonModalControllerElement;
  }
  interface PlayerList extends JSXBase.HTMLAttributes<HTMLPlayerListElement> {}
  interface PlayerListItem extends JSXBase.HTMLAttributes<HTMLPlayerListItemElement> {
    'index'?: number;
    'onPlayerEvent'?: (event: CustomEvent<any>) => void;
    'player'?: any;
    'slideEnabled'?: boolean;
  }

  interface IntrinsicElements {
    'app-home': AppHome;
    'app-root': AppRoot;
    'player-create': PlayerCreate;
    'player-list': PlayerList;
    'player-list-item': PlayerListItem;
  }
}

export { LocalJSX as JSX };


declare module "@stencil/core" {
  export namespace JSX {
    interface IntrinsicElements extends LocalJSX.IntrinsicElements {}
  }
}


